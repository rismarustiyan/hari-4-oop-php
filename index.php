<?php
    require_once ('animal.php');
    require_once ('frog.php');
    require_once ('ape.php');
    $sheep = new animal("shaun");

    echo "Name: ". $sheep->name."<br>";
    echo "Legs: " . $sheep->legs."<br>" ;
    echo "Cold_blooded: " . $sheep->cold_blooded. "<br>";
    echo "<br>";

    $kodok = new frog("buduk");

    echo "Name: ". $kodok->name."<br>";
    echo "Legs: " . $kodok->legs."<br>" ;
    echo "Cold_blooded: " . $kodok->cold_blooded. "<br>";
    echo "Jump: ";
    $kodok->jump();
    echo "<br><br>";

    $sungokong = new ape("kera sakti");

    echo "Name: ". $sungokong->name."<br>";
    echo "Legs: " . $sungokong->legs."<br>" ;
    echo "Cold_blooded: " . $sungokong->cold_blooded. "<br>";
    echo "Yell: ";
    $sungokong->yell();
?>